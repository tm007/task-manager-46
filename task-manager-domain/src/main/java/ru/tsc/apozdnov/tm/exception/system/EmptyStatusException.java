package ru.tsc.apozdnov.tm.exception.system;

import ru.tsc.apozdnov.tm.exception.AbstractException;

public final class EmptyStatusException extends AbstractException {

    public EmptyStatusException() {
        super("Error! Status is empty...");
    }

}